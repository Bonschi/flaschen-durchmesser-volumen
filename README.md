# Flaschen Töpfe Durchmesser Volumen

Aufgabe mit den bekannten Volumen für die Gefäßformen Topf und Flasche
1. für beide mittel der Funktion `lm()` ein lineares Modell erstellen, daraus die Formel für die Regressionsgerade erstellen
2. Ein Streudiagramm mit `geom_point` erstellen, die Regression(sgerade) mit `geom_smoth()` einzeichnen
3. Konfidentintervall mit `se=TRUE` im Plot anzeigen lassen
4. die Gefäßformen in Farben und Formen trennen
5. facets für die Gefäßformen erstellen
