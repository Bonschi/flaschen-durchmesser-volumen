# Review nach Abgabe 06.05.2021

## Probleme mit der aktuellen Vesion

- Das Umrechnen der Radien in Durchmesser ist nicht notwendig
- wenn Sie sich die Verteilung der Flaschen ansehen, erkennen Sie einen 
  Unterschied zu den Einzeldiagrammen. Warum?
    - Der Unterschied zwischen dem Einzeldiagramm und dem gemeinsamen Diagramm
      entsteht dadurch, dass ich bei dem Einzeldiagramm den max. Durchmesser und
      bei dem gemeinsamen Diagramm den Randdruchmesser verwendet habe.
      Ich habe diesen Unterschied gemacht, da ich die beiden Formen in einem
      Diagramm darstellen wollte.
      F�r die Untersuchung der Flaschen Untersuchung ist das nat�rlich wenig 
      zielf�hrend.
      Im "Zahlenspiele" Aufsatz hei�t es n�mlich:
      
      > "Sicher ist der maximale Druchmesser im Bauch, der Bauchpunkt, das 
      > Charakteristikum eines Gef��es.
      > Es ist der Punkt, der Das Volumen am st�rksten beeinflusst [...]."
      > (S. 208)
      
      Vor allem bei den Flaschen wo der Druchmesser des Halses f�r das Volumen
      kaum von Bedeutung ist, auch mit Hinblick auf die Diskussion �ber den 
      F�llstand, ist es also nicht sinnvoll diesn mit dem Volumen zu 
      korrelieren.
      Beim da dieser Datensatz sowieso nur vollst�ndige Untersuchungseinheiten
      enth�lt, k�nnen die T�pfe, bei denen das Volumen ohnehin ebenfalls durch
      den Bauchdurchmesser massiv beeinflusst wird, ebenfalls auf diese Variable
      hin untersucht werden.
      
# �nderungen f�r die n�chste Version

- Radien nicht in Durchmesser umrechnen
- Sowohl f�r T�pfe, als auch f�r Flaschen den max. Radius verwenden
- Kurvenlineare Regressionsline einzeichnen
    - Ich denke, dass ich die Variable `formel` in `geom_smooth()` anpassen muss,
      bin mir jedoch nicht sicher wie das geht, bzw. welche Rechenoperationen
      erlaubt sind.

